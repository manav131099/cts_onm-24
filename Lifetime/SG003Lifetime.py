import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys

tz = pytz.timezone('Asia/Singapore')
path='/home/admin/Dropbox/Second Gen/[SG-003S]'
path2='/home/admin/Dropbox/Lifetime/'


if(os.path.exists(path2+"SG-003-LT.txt")):
    pass
else:
    list2=[]
    for i in sorted(os.listdir(path)):
        for j in sorted(os.listdir(path+'/'+i)):
            for k in sorted(os.listdir(path+'/'+i+'/'+j)):
                if(len(k)==18):
                    df=pd.read_csv(path+'/'+i+'/'+j+'/'+k,sep='\t')
                    df1 = df[['Date','Gsi00','Eac2MA','Eac2MB','Eac2MC','LastR1','LastR2','LastR3','LastT1','LastT2','LastT3','DA']].copy()
                    df1.columns=['Date','GHI','EacMA','EacMB','EacMC','LastR-MA','LastR-MB','LastR-MC','LastT','LastT','LastT','DA']
                    df1=df1[['Date','GHI','LastR-MA','LastR-MB','LastR-MC','EacMA','EacMB','EacMC','LastT','DA']]
                    print(df1)
                    df1.insert(2, "GHI-Flag", 0)
                    df1.insert(4, "LastR-MA-Flag", 0)
                    df1.insert(6, "LastR-MB-Flag", 0)   
                    df1.insert(8, "LastR-MC-Flag", 0)
                    df1['Master-Flag']=0
                    if(os.path.exists(path2+"SG-003-LT.txt")):
                        df1.to_csv(path2+'SG-003-LT.txt',sep='\t',mode='a',index=False,header=False)
                    else:
                        df1.to_csv(path2+'SG-003-LT.txt',sep='\t',mode='a',index=False,header=True)

#Historical
df2=pd.read_csv(path2+'SG-003-LT.txt',sep='\t')
startdate=(df2['Date'][len(df2['Date'])-1])
start=startdate
start=datetime.datetime.strptime(start, "%Y-%m-%d").date()
print(start)
end=datetime.datetime.now(tz).date()
print(end)
tot=end-start
tot=tot.days
print('Diff day is,',tot)
for n in range(tot):
    strstart=str(start)
    df=pd.read_csv(path+'/'+strstart[0:4]+'/'+strstart[0:7]+'/[SG-003S] '+strstart[2:4]+strstart[5:7]+'.txt',sep='\t')
    df1 = df[['Date','Gsi00','Eac2MA','Eac2MB','Eac2MC','LastR1','LastR2','LastR3','LastT1','LastT2','LastT3','DA']].copy()
    df1.columns=['Date','GHI','EacMA','EacMB','EacMC','LastR-MA','LastR-MB','LastR-MC','LastT','LastT','LastT','DA']
    df1=df1[['Date','GHI','LastR-MA','LastR-MB','LastR-MC','EacMA','EacMB','EacMC','LastT','DA']]
    df1.insert(2, "GHI-Flag", 0)
    df1.insert(4, "LastR-MA-Flag", 0)
    df1.insert(6, "LastR-MB-Flag", 0)   
    df1.insert(8, "LastR-MC-Flag", 0)
    df1['Master-Flag']=0
    if((df2['Date']==strstart).any()):
        print("Skipped")
        pass
    else:
        df3=df1.loc[df1['Date']==strstart]
        df3.to_csv(path2+'SG-003-LT.txt',sep='\t',mode='a',index=False,header=False)
        print(df3)
    start=start+datetime.timedelta(days=1)

#Live
while(1):
    timenow=datetime.datetime.now(tz)
    timenowstr=str(timenow)
    timenowdate=str(timenow.date())
    df=pd.read_csv(path+'/'+timenowstr[0:4]+'/'+timenowstr[0:7]+'/[SG-003S] '+timenowstr[2:4]+timenowstr[5:7]+'.txt',sep='\t')
    df2=pd.read_csv(path2+'SG-003-LT.txt',sep='\t')
    df1 = df[['Date','Gsi00','Eac2MA','Eac2MB','Eac2MC','LastR1','LastR2','LastR3','LastT1','LastT2','LastT3','DA']].copy()
    df1.columns=['Date','GHI','EacMA','EacMB','EacMC','LastR-MA','LastR-MB','LastR-MC','LastT','LastT','LastT','DA']
    df1=df1[['Date','GHI','LastR-MA','LastR-MB','LastR-MC','EacMA','EacMB','EacMC','LastT','DA']]
    df1.insert(2, "GHI-Flag", 0)
    df1.insert(4, "LastR-MA-Flag", 0)
    df1.insert(6, "LastR-MB-Flag", 0)   
    df1.insert(8, "LastR-MC-Flag", 0)
    df1['Master-Flag']=0
    if((df2['Date']==timenowdate).any()):
        print("Skipped")
        pass
    else:
        df3=df1.loc[df1['Date']==timenowdate]
        if(df3.empty):
            print("Waiting")
        else:
            df3.to_csv(path2+'SG-003-LT.txt',sep='\t',mode='a',index=False,header=False)
            print(df3)
    time.sleep(3600)
