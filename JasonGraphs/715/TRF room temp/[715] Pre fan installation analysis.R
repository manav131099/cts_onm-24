################################################################################################
rm(list=ls(all =TRUE))
library(ggplot2)

coerce_num <- function(x)
{
  v1 <- as.numeric(as.character(x))
  return(v1)
}

lm_eqn <- function(x,y,data)
{
  m <- lm(y ~ x, data);
  eq <- substitute(italic(y) == a + b %.% italic(x)*","~~italic(r)^2~"="~r2, 
                   list(a = format(coef(m)[1], digits = 2), 
                        b = format(coef(m)[2], digits = 2), 
                        r2 = format(summary(m)$r.squared, digits = 3)))
  as.character(as.expression(eq));                 
}

#enter station directory
pathRead <- "~/intern/Data/[715]/2018/2018-03/"
setwd(pathRead)

filelist <- dir(pattern = ".txt", recursive = TRUE)

gsicol <- 3
troomcol <- 4
tambcol <- 9
tmod <- 8

timemin <- format(as.POSIXct("2016-07-14 06:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-11-02 17:00:00"), format="%H:%M:%S")

df2=NULL
for(i in filelist){
  df = NULL
  temp <- read.table(i,header = T, sep = "\t")
  condition <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin &
    format(as.POSIXct(temp[,1]), format="%H:%M:%S") <timemax
  temp2 <- temp[condition,]
  date <- substr(i,7,16)                
  df1 <- cbind(as.character(temp[,1]), temp[,gsicol], temp[,troomcol], temp[,tambcol], temp[,tmod])
  df2 <- rbind(df2,df1)
  print(i)
}

df2 <- as.data.frame(df2)

#colnames(df2) <- c('datetime', 'Gsi00', 'Troom', 'Tamb', 'Tmod')

#
for(i in 2:5)
{
  df2[,i] <- coerce_num(df2[,i])
}

df2[,6] <- df2[,5]-df2[,3]
df2[,7] <- df2[,4]-df2[,3]
colnames(df2) <- c('datetime', 'Gsi00', 'Troom', 'Tamb', 'Tmod', 'Tmod_Troom', 'Tamb_Troom')

line <-  lm(df2[,3] ~ df2[,2])

int= as.numeric(format(coef(line)[1]))
s=as.numeric(format(coef(line)[2]))

themesettings1 <- theme(plot.title = element_text(hjust = 0.5),                #title alignment
                        axis.title.x = element_text(size=19),                  # x axis label size
                        axis.title.y = element_text(size=19),                  # y axis label size
                        axis.text = element_text(size=11),                     # axes text size
                        panel.grid.minor = element_blank(),                    #remove minor grid lines
                        panel.border = element_blank(),                        #remove plot border
                        axis.line = element_line(colour = "black"),            # axis line colour
                        legend.justification = c(1, 1),                        #legend top right
                        legend.position = c(0.1, 1))                             #legend inside graph

titlesetting <-  theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
                       plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5))

g1 <- ggplot(data = df2, aes(x=Gsi00, y=Troom)) + theme_bw() + geom_point(colour = "grey", size = 0.7)
g1 <- g1 + ylab('Troom [�C]') + xlab('Gsi00 [W/m�]') + ggtitle('1. Room temp w/ relation to Irradiance before fans installed', subtitle = 'March 2018') + themesettings1
g1 <- g1 + scale_y_continuous(breaks=seq(25, 60, 5)) + titlesetting
g1 <- g1 + scale_x_continuous(breaks=seq(0,1200,200)) + coord_cartesian(ylim = c(25,50), xlim = c(0,1200))
g1 <- g1 + geom_smooth(aes(x=Gsi00, y=Troom, colour = "Linear fit"), method = "lm") + scale_colour_manual("", values=c("Linear fit" = "blue"))
g1 <- g1 + annotate('text', label = paste0("y = ", round(int,2), " + ", round(s,2),"x"), x =  1100, y = 28, colour = "blue")
g1

write.table(df2, na = "", 'C:/Users/talki/Desktop/cec intern/results/715/Troom against Irradiance Mar-18.txt' ,row.names = FALSE,sep ="\t")
ggsave('C:/Users/talki/Desktop/cec intern/results/715/1. [Pre fan installation] Troom against Irradiance Mar-18.pdf', g1, width =12, height=6)

line <-  lm(df2[,6] ~ df2[,2])

int= as.numeric(format(coef(line)[1]))
s=as.numeric(format(coef(line)[2]))

g2 <- ggplot(data=df2, aes(x=Gsi00, y=Tmod_Troom)) + theme_bw() + geom_point(colour = "orange2", size = 0.8)
g2 <- g2 + ylab(expression(Delta*'T = Tmod-Troom [�C]')) + xlab('Gsi [W/m�]') 
g2 <- g2 + ggtitle('2. Temperature w/ relation to Irradiance before installation of fans', subtitle = 'March 2018') + themesettings1 + titlesetting
g2 <- g2 + scale_y_continuous(breaks=seq(-5, 50, 5))
g2 <- g2 + scale_x_continuous(breaks=seq(0,1200,200))
g2 <- g2 + geom_smooth(aes(x=Gsi00, y=Tmod_Troom, colour = "Linear fit"), method = "lm") + scale_colour_manual("", values=c("Linear fit" = "blue"))
g2 <- g2 + annotate('text', label = paste0("y = ", round(int,2), " + ", round(s,3),"x"), x =  1100, y = 4, colour = "blue")
g2

line <-  lm(df2[,7] ~ df2[,2])

int= as.numeric(format(coef(line)[1]))
s=as.numeric(format(coef(line)[2]))

g3 <- ggplot(data=df2, aes(x=Gsi00, y=Tamb_Troom)) + theme_bw() + geom_point(colour = "red2", size = 0.8)
g3 <- g3 + ylab(expression(Delta*'T = Tamb-Troom [�C]')) + xlab('Gsi [W/m�]') 
g3 <- g3 + ggtitle('3. Temperature w/ relation to Irradiance before installation of fans', subtitle = 'March 2018') + themesettings1 + titlesetting
g3 <- g3 + scale_y_continuous(breaks=seq(-10, 20, 5))
g3 <- g3 + scale_x_continuous(breaks=seq(0,1200,200))
g3 <- g3 + geom_smooth(aes(x=Gsi00, y=Tamb_Troom, colour = "Linear fit"), method = "lm") + scale_colour_manual("", values=c("Linear fit" = "blue"))
g3 <- g3 + annotate('text', label = paste0("y = ", round(int,2), " + ", round(s,3),"x"), x =  1100, y = -8, colour = "blue")
g3

ggsave('C:/Users/talki/Desktop/cec intern/results/715/2. [Pre fan installation] (Tmod-Troom) against Irradiance before fans installed.pdf', g2, width =12, height=6)
ggsave('C:/Users/talki/Desktop/cec intern/results/715/3. [Pre fan installation] (Tamb-Troom) against Irradiance before fans installed.pdf', g3, width =12, height=6)

