rm(list=ls(all =TRUE))
library(ggplot2)
library(zoo)
result <- read.csv("/home/admin/Jason/cec intern/results/715/[715]_summary.csv")
rownames(result) <- NULL
result <- data.frame(result)
result <- result[,-1]
colnames(result) <- c("date","da","pts",'gsi',"eac1","eac2","pr1","pr2")
result[,1] <- as.Date(result[,1], origin = result[,1][1])
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))
result[,5] <- as.numeric(paste(result[,5]))
result[,6] <- as.numeric(paste(result[,6]))
result[,7] <- as.numeric(paste(result[,7]))
result[,8] <- as.numeric(paste(result[,8]))


zoo.pr1 <- zoo(result[,7], as.Date(result$date))
ma1 <- rollapplyr(zoo.pr1,list(-(29:1)), mean, fill=NA, na.rm=T)
result$ambpr1.av=coredata(ma1)

zoo.pr2 <- zoo(result[,8], as.Date(result$date))
ma2 <- rollapplyr(zoo.pr2,list(-(29:1)), mean, fill=NA, na.rm=T)
result$ambpr2.av=coredata(ma2)

result$colourcode = 0
result[,11][result[,4] < 3] <- "< 3"
result[,11][result[,4] >= 3 & result[,4] <= 6] <- "3 ~ 6"
result[,11][result[,4] > 6] <- "> 6"
result$colourcode = factor(result$colourcode, levels = c("< 3", "3 ~ 6", "> 6"), labels =c('< 3','3 ~ 6','> 6') )       #to fix legend arrangement

#cols <- c("< 3" = "blue",  "3 ~ 6" = "lightblue", "> 6" = "darkorange1")
  
p1 <- ggplot(data=result, aes(x=as.Date(date), y=pr1, colour = colourcode)) + theme_bw()
p1 <- p1 + geom_point() 
p1 <- p1 + ylab("Performance Ratio [%]") + xlab("Time") + coord_cartesian(ylim = c(0,100))
p1 <- p1 + scale_x_date(date_breaks = "1 month",date_labels = "%b")
p1 <- p1 + scale_y_continuous(breaks=seq(0, 100, 10))
p1 <- p1 +  theme(plot.title = element_text(hjust = 0.5), axis.title.x = element_text(size=19), axis.title.y = element_text(size=19),
                  axis.text = element_text(size=13), panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black"),
                  legend.justification = c(1, 1), legend.position = c(1, 1))
p1 <- p1 + scale_colour_manual('Irradiation [kWh/m2]', values = c("blue", "deepskyblue1", "darkorange3"), labels = c('< 3','3 ~ 6','> 6')) 
p1 <- p1 + geom_hline(yintercept = 75, colour = "black", size = 1.1) + geom_hline(yintercept = 68, colour = "darkgreen", size = 1.1)
p1 <- p1 + geom_hline(yintercept = 60, colour = "green1", size= 1.1) + geom_hline(yintercept = 50, colour = "darkorange2", size =1.1) + geom_hline(yintercept = 40, colour = "gold1", size= 1.1)
p1 <- p1 + geom_line(data=result, aes(x=as.Date(date), y=ambpr1.av),color="red", size=1.5)                              
p1 <- p1 + annotate('text', label = "30-d moving average of PR", y = 26 , x = as.Date(result[101,1]), colour = "red")
p1 <- p1 + annotate("segment", x = as.Date(result[110,1]), xend = as.Date(result[110,1]), y = 28, yend = 52 , colour = "red", size=1.5, alpha=0.5, arrow=arrow())
p1

p2 <- ggplot() + theme_bw()
p2 <- p2 + geom_point(data=result, aes(x=as.Date(date), y=pr2, colour = colourcode)) 
p2 <- p2 + ylab("Performance Ratio [%]") + xlab("Time") + coord_cartesian(ylim = c(0,100))
p2 <- p2 + scale_x_date(date_breaks = "1 month",date_labels = "%b")
p2 <- p2 + scale_y_continuous(breaks=seq(0, 100, 10))
p2 <- p2 +  theme(plot.title = element_text(hjust = 0.5), axis.title.x = element_text(size=19), axis.title.y = element_text(size=19),
                  axis.text = element_text(size=13), panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black"),
                  legend.justification = c(1, 1), legend.position = c(1, 1))
p2 <- p2 + geom_hline(yintercept = 75, colour = "black", size = 1.1) + geom_hline(yintercept = 68, colour = "darkgreen", size = 1.1)
p2 <- p2 + geom_hline(yintercept = 60, colour = "green1", size= 1.1) + geom_hline(yintercept = 50, colour = "darkorange2", size =1.1) + geom_hline(yintercept = 40, colour = "gold1", size= 1.1)
p2 <- p2 + geom_line(data=result, aes(x=as.Date(date), y=ambpr2.av),color="red", size=1.5) 
p2 <- p2 + scale_colour_manual('Irradiation [kWh/m2]', values = c("blue", "deepskyblue", "darkorange3"), labels = c('< 3','3 ~ 6','> 6')) 
p2 <- p2 + annotate('text', label = "30-d moving average of PR", y = 25 , x = as.Date(result[101,1]), colour = "red")
p2 <- p2 + annotate("segment", x = as.Date(result[110,1]), xend = as.Date(result[110,1]), y = 28, yend = 52 , colour = "red", size=1.5, alpha=0.5, arrow=arrow())


ggsave(paste0('/home/admin/Jason/cec intern/results/715/PR1.pdf'), p1, width =12, height=6)
ggsave(paste0('/home/admin/Jason/cec intern/results/715/PR2.pdf'), p2, width =12, height=6)


