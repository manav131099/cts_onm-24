source('/home/admin/CODE/common/aggregate.R')

registerMeterList("IN-010X",c(""))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 4 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 6 #column for Yld-1
aggColTemplate[8] = 7 #column for Yld-2
aggColTemplate[9] = 9 #column for PR-1
aggColTemplate[10] = 10 #column for PR-2
aggColTemplate[11] = 8 #column for Irr
aggColTemplate[12] = "IN-711" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("IN-010X","",aggNameTemplate,aggColTemplate)
