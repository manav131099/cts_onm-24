source('/home/admin/CODE/common/aggregate.R')

registerMeterList("KH-008X",c("Pond","R02","R11","R03","R06"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = 6 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 4 #column for Eac-1
aggColTemplate[6] = 5 #column for Eac-2
aggColTemplate[7] = 8 #column for Yld-1
aggColTemplate[8] = 9 #column for Yld-2
aggColTemplate[9] = 11 #column for PR-1
aggColTemplate[10] = 12 #column for PR-2
aggColTemplate[11] = 10 #column for Irr
aggColTemplate[12] = "KH-008S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-008X","Pond",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = 6 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 4 #column for Eac-1
aggColTemplate[6] = 5 #column for Eac-2
aggColTemplate[7] = 8 #column for Yld-1
aggColTemplate[8] = 9 #column for Yld-2
aggColTemplate[9] = 11 #column for PR-1
aggColTemplate[10] = 12 #column for PR-2
aggColTemplate[11] = 10 #column for Irr
aggColTemplate[12] = "KH-008S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-008X","R02",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = 6 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 4 #column for Eac-1
aggColTemplate[6] = 5 #column for Eac-2
aggColTemplate[7] = 8 #column for Yld-1
aggColTemplate[8] = 9 #column for Yld-2
aggColTemplate[9] = 11 #column for PR-1
aggColTemplate[10] = 12 #column for PR-2
aggColTemplate[11] = 10 #column for Irr
aggColTemplate[12] = "KH-008S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-008X","R11",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = 6 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 4 #column for Eac-1
aggColTemplate[6] = 5 #column for Eac-2
aggColTemplate[7] = 8 #column for Yld-1
aggColTemplate[8] = 9 #column for Yld-2
aggColTemplate[9] = 11 #column for PR-1
aggColTemplate[10] = 12 #column for PR-2
aggColTemplate[11] = 10 #column for Irr
aggColTemplate[12] = "KH-008S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-008X","R03",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = 6 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 4 #column for Eac-1
aggColTemplate[6] = 5 #column for Eac-2
aggColTemplate[7] = 8 #column for Yld-1
aggColTemplate[8] = 9 #column for Yld-2
aggColTemplate[9] = 11 #column for PR-1
aggColTemplate[10] = 12 #column for PR-2
aggColTemplate[11] = 10 #column for Irr
aggColTemplate[12] = "KH-008S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-008X","R06",aggNameTemplate,aggColTemplate)

