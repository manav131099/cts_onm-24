errHandle = file('/home/admin/Logs/Logs714Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

system('rm -R "/home/admin/Dropbox/Second Gen/[KH-714S]"')
source('/home/admin/CODE/714Digest/runHistory714.R')
rm(list = ls())
require('mailR')
print('History done')
source('/home/admin/CODE/714Digest/3rdGenData714.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/714Digest/aggregateInfo.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
print('3rd gen data called')
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
  min = as.numeric(hr[2])
  hr = as.numeric(hr[1])
  return((hr * 60 + min + 1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
prepareSumm = function(dataread)
{
  da = nrow(dataread)
  thresh = 5
  gsi1 = sum(dataread[complete.cases(dataread[,3]),3])/60000
  gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
	gsi3 = sum(dataread[complete.cases(dataread[,5]),5])/60000
  gsismp = sum(dataread[complete.cases(dataread[,6]),6])/60000
  subdata = dataread[complete.cases(dataread[,3]),]
  subdata = subdata[as.numeric(subdata[,3]) > thresh,]
  tamb = mean(dataread[complete.cases(dataread[,8]),8])
  tambst = mean(subdata[,8])
  
  hamb = mean(dataread[complete.cases(dataread[,7]),7])
  hambst = mean(subdata[,7])
  
  tambmx = max(dataread[complete.cases(dataread[,8]),8])
  tambstmx = max(subdata[,8])
  
  hambmx = max(dataread[complete.cases(dataread[,7]),7])
  hambstmx = max(subdata[,7])
  
  tambmn = min(dataread[complete.cases(dataread[,8]),8])
  tambstmn = min(subdata[,8])
  
  hambmn = min(dataread[complete.cases(dataread[,7]),7])
  hambstmn = min(subdata[,7])
  
  tsi1 = mean(dataread[complete.cases(dataread[,3]),9])
  tsi2 = mean(dataread[complete.cases(dataread[,3]),10])
  
  gsirat1 = gsi2 / gsi1
	gsirat2 = gsi3 / gsi1
  smprat = gsismp / gsi1
	daPerc = da/14.4
  
  datawrite = data.frame(Date = substr(dataread[1,1],1,10),PtsRec = da,Gsi = rf(gsi1), GModE = rf(gsi2),GModW=rf(gsi3),Smp = rf(gsismp),
                         Tamb = rf1(tamb), TambSH = rf1(tambst),TambMx = rf1(tambmx), TambMn = rf1(tambmn),
                         TambSHmx = rf(tambstmx), TambSHmn = rf1(tambstmn), Hamb = rf1(hamb), HambSH = rf1(hambst),
                         HambMx = rf1(hambmx), HambMn = rf1(hambmn), HambSHmx = rf1(hambstmx), HambSHmn = rf1(hambstmn),
                         Tsi01 = rf1(tsi1), Tsi02= rf1(tsi2), GModERat = rf3(gsirat1),GModWRat=rf3(gsirat2), SpmRat = rf3(smprat),DA=rf1(daPerc),stringsAsFactors=FALSE)
  datawrite
}
rewriteSumm = function(datawrite)
{
  
  df = data.frame(Date = as.character(datawrite[1,1]),Gsi = as.character(datawrite[1,3]),GModE = as.character(datawrite[1,4]),GModW=as.character(datawrite[1,5]),Smp = as.character(datawrite[1,6]),
                  Tamb = as.character(datawrite[1,7]),TambSH = as.character(datawrite[1,8]),Hamb = as.character(datawrite[1,13]),HambSH = as.character(datawrite[,14]),
									DA=as.character(datawrite[,24]),stringsAsFactors=FALSE)
  df
}


preparebody = function(path)
{
  print("Enter Body Func")
  body = ""
	body = paste(body,"Meteorological Station Name: KH-714 Phnom Penh\n",sep="")
	body = paste(body,"\n Location: Phnom Penh, Cambodia\n")
	body = paste(body,"\n Coordinates: 11.508282, 104.775464\n")     #line added
	body = paste(body,"\n O&M Code: GND-PHNO\n")
	body = paste(body,"\n Monitoring System Code: KH-714\n")
	body = paste(body,"\n Yearly Long-Term GHI: 1,917 kWh/m2 (source: SolarGIS)\n")
	body = paste(body,"\n Monthly Long-Term GHI (daily averages in brackets)\n")
	body = paste(body,"\n\tJan: 157.5 kWh/m2 (5.08)\n")
	body = paste(body,"\n\tFeb: 159.4 kWh/m2 (5.69)\n")
	body = paste(body,"\n\tMar: 181.2 kWh/m2 (5.85)\n")
	body = paste(body,"\n\tApr: 174.2 kWh/m2 (5.81)\n")
	body = paste(body,"\n\tMay: 176.3 kWh/m2 (5.69)\n")
	body = paste(body,"\n\tJun: 156.1 kWh/m2 (5.20)\n")
	body = paste(body,"\n\tJul: 155.3 kWh/m2 (5.01)\n")
	body = paste(body,"\n\tAug: 162.8 kWh/m2 (5.25)\n")
	body = paste(body,"\n\tSep: 142.8 kWh/m2 (4.76)\n")
	body = paste(body,"\n\tOct: 148.6 kWh/m2 (4.79)\n")
	body = paste(body,"\n\tNov: 145.2 kWh/m2 (4.84)\n")
	body = paste(body,"\n\tDec: 157.4 kWh/m2 (5.08)\n")
	body = paste(body,"\n Station First Day of Operations: 2016-06-05\n")
	body = paste(body,"\n System age [days]:",DAYSACTIVE,"\n")
	body = paste(body,"\n System age [years]:",rf1(DAYSACTIVE/365))

  for(x in 1 : length(path))
  {
		print(path[x])
		body = paste(body,"\n\n_____________________________________________\n")
		days = unlist(strsplit(path[x],"/"))
		days = substr(days[length(days)],11,20)
		body = paste(body,days)
		body  = paste(body,"\n_____________________________________________\n")
		dataread = read.table(path[x],header = T,sep="\t")
		body = paste(body,"\nPts Recorded: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,2])/14.4,1),"%)",sep="")
		body = paste(body,"\n\nDaily Irradiation Pyranometer, GHI [kWh/m2]:",as.character(dataread[1,6]))
		body = paste(body,"\n\nDaily Irradiation Silicon Sensor, GHI [kWh/m2]:",as.character(dataread[1,3]))
		body = paste(body,"\n\nDaily Irradiation Silicon Sensor, East 10-deg [kWh/m2]:",as.character(dataread[1,4]))
		body = paste(body,"\n\nDaily Irradiation Silicon Sensor, West 10-deg [kWh/m2]:",as.character(dataread[1,5]))
		body = paste(body,"\n\nMean Ambient Temperature [C]:",as.character(dataread[1,7]))
		body = paste(body,"\n\nMean Ambient Temperature solar hours [C]:",as.character(dataread[1,8]))
		body = paste(body,"\n\nMax Ambient Temperature [C]:",as.character(dataread[1,9]))
		body = paste(body,"\n\nMin Ambient Temperature solar hours [C]:",as.character(dataread[1,10]))
		body = paste(body,"\n\nMean Humidity [%]:",as.character(dataread[1,13]))
		body = paste(body,"\n\nMean Humidity solar hours [%]:",as.character(dataread[1,14]))
		body = paste(body,"\n\nMax Humidity [%]:",as.character(dataread[1,15]))
		body = paste(body,"\n\nMin Humidity solar hours [%]:",as.character(dataread[1,16]))
		body = paste(body,"\n\nGmodE/Gsi Ratio:",as.character(dataread[1,21]))
		body = paste(body,"\n\nGmodW/Gsi Ratio:",as.character(dataread[1,22]))
		body = paste(body,"\n\nPyr/Si Ratio:",as.character(dataread[1,23]))
  }
print('Daily Summary for Body Done')
		body = paste(body,"\n\n_____________________________________________\n\n")

  body = paste(body,"Station History")
		body = paste(body,"\n\n_____________________________________________\n\n")

	body = paste(body,"Station Birth Date:",dobstation)
	body = paste(body,"\n\n# Days station active:",DAYSACTIVE)
	body = paste(body,"\n\n# Years station active:",rf1(DAYSACTIVE/365))
	body = paste(body,"\n\nTotal irradiation from Si sensor this month [kWh/m2]:",MONTHLYIRR1)
	body = paste(body,"\n\nTotal irradiation from 10E sensor this month [kWh/m2]:",MONTHLYIRR2)
	body = paste(body,"\n\nTotal irradiation from 10W sensor this month [kWh/m2]:",MONTHLYIRR3)
	body = paste(body,"\n\nTotal irradiation  measured by Pyranometer [kWh/m2]:",MONTHLYIRR4)
	 body = paste(body,"\n\nAverage irradiation from Si sensor this month [kWh/m2]:",MONTHAVG1)
	   body = paste(body,"\n\nAverage irradiation from Si sensor 10E this month [kWh/m2]:",MONTHAVG2)
	   body = paste(body,"\n\nAverage irradiation from Si sensor 10W this month [kWh/m2]:",MONTHAVG3)
		   body = paste(body,"\n\nAverage irradiation measured by Pyranometer this month [kWh/m2]:",MONTHAVG4)
	body = paste(body,"\n\nForecasted irradiation total for Si sensor this month [kWh/m2]:",FORECASTIRR1);
	body = paste(body,"\n\nForecasted irradiance total for 10E Si sensor this month [kWh/m2]:",FORECASTIRR2);
	body = paste(body,"\n\nForecasted irradiance total for 10W Si sensor this month [kWh/m2]:",FORECASTIRR3);
	body = paste(body,"\n\nForecasted irradiance total for Pyranamoter this month [kWh/m2]:",FORECASTIRR4)
	body = paste(body,"\n\nIrradiation total last 30 days from Si sensor [kWh/m2]:",LAST30DAYSTOT)
  body = paste(body,"\n\nIrradiation total last 30 days from Pyranometer [kWh/m2]:",LAST30DAYSTOT2)
	body = paste(body,"\n\nIrradiation average last 30 days from Si sensor [kWh/m2]:",LAST30DAYSMEAN)
	body = paste(body,"\n\nIrradiance avg last 30 days from Pyranometer[kWh/m2]:",LAST30DAYSMEAN2)
	body = paste(body,"\n\nGmodE/GSI loss (%):",SOILINGDEC)
	body = paste(body,"\n\nGmodE/GSI loss per day (%)",SOILINGDECPD)
	body = paste(body,"\n\nGmodW/GSI loss (%):",SOILINGDEC2)
	body = paste(body,"\n\nGmodW/GSI loss per day (%)",SOILINGDECPD2)
	body = paste(body,"\n\nPyr/Si loss (%):",SNPDEC)
	body = paste(body,"\n\nPyr/Si loss per day (%)",SNPDECPD)
print('3G Data Done')	
return(body)

}
path = "/home/admin/Dropbox/Cleantechsolar/1min/[714]"
pathwrite = "/home/admin/Dropbox/Second Gen/[KH-714S]"
checkdir(pathwrite)
date = format(Sys.time(),tz = "Singapore")
years = dir(path)
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevsumname = paste("[KH-714S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")
#if(length(dir(pathmonth)) > 1)
#{
#  prevday = dir(pathmonth[length(dir(pathmonth))-1])
#}
stnnickName = "714"
stnnickName2 = "KH-714S"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
if(!is.null(lastdatemail))
{
	yr = substr(lastdatemail,1,4)
	monyr = substr(lastdatemail,1,7)
	prevpathyear = paste(path,yr,sep="/")
	prevsumname = paste("[",stnnickName2,"] ",substr(lastdatemail,3,4),substr(lastdatemail,6,7),".txt",sep="")
	prevpathmonth = paste(prevpathyear,monyr,sep="/")
	currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
	prevwriteyears = paste(pathwrite,yr,sep="/")
	prevwritemonths = paste(prevwriteyears,monyr,sep="/")
}

check = 0
sender <- "operations@cleantechsolar.com"
uname <- "shravan.karthik@cleantechsolar.com"
pwd = 'CTS&*(789'
recipients = getRecipients("KH-714S","m")

wt =1
while(1)
{
recordTimeMaster("KH-714S","Bot")
recipients = getRecipients("KH-714S","m")
years = dir(path)
writeyears = paste(pathwrite,years[length(years)],sep="/")
checkdir(writeyears)
pathyear = paste(path,years[length(years)],sep="/")
months = dir(pathyear)
writemonths = paste(writeyears,months[length(months)],sep="/")
sumname = paste("[KH-714S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
checkdir(writemonths)
pathmonth = paste(pathyear,months[length(months)],sep="/")
tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
currday = dir(pathmonth)[length(dir(pathmonth))]
print(paste('Currday is',currday))
print(paste('Prevday is',prevday))

if(length(currday) < 1)
{
print('Empty.. sleeping for an hour')
Sys.sleep(3600)
next
}

if(prevday == currday)
{
   if(wt == 1)
{
 print("")
 print("")
 print("__________________________________________________________")
 print(substr(currday,7,16))
 print("__________________________________________________________")
 print("")
 print("")
 print('Waiting')
wt = 0
}
{
  if(tm > 1319 || tm < 30)
  {
      Sys.sleep(120)
  }        
  else
  {
    Sys.sleep(3600)
  }
}
next
}
print('New Data found.. sleeping to ensure sync complete')
Sys.sleep(20)
print('Sleep Done')
if(prevpathmonth != pathmonth)
{
	monthlyirr1 = monthlyirr2 = monthlyirr3 = monthlyirr4 = 0
  strng = unlist(strsplit(months[length(months)],"-"))
	daystruemonth = nodays(strng[1],strng[2])
	dayssofarmonth = 0
}

dataread = read.table(paste(pathmonth,currday,sep='/'),header = T,sep = "\t")
datawrite = prepareSumm(dataread)
datasum = rewriteSumm(datawrite)
currdayw = gsub("714","KH-714S",currday)
write.table(datawrite,file = paste(writemonths,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)



monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3])
monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4])
monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5])
monthlyirr4 = monthlyirr4 + as.numeric(datawrite[1,6])

globirr1 = globirr1 + as.numeric(datawrite[1,3])
globirr2 = globirr2 + as.numeric(datawrite[1,4])
globirr3 = globirr3 + as.numeric(datawrite[1,5])
globirr4 = globirr4 + as.numeric(datawrite[1,6])

dayssofarmonth = dayssofarmonth + 1    
daysactive = daysactive + 1
last30days[[last30daysidx]] = as.numeric(datawrite[1,3])
last30days2[[last30daysidx]] = as.numeric(datawrite[1,6])



print('Digest Written');
{
  if(!file.exists(paste(writemonths,sumname,sep="/")))
  {
    write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    print('Summary file created')
}
  else 
  {
    write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
   print('Summary file updated')  
}
}
filetosendpath = c(paste(writemonths,currdayw,sep="/"))
filename = c(currday)
filedesc = c("Daily Digest")
dataread = read.table(paste(prevpathmonth,prevday,sep='/'),header = T,sep = "\t")
datawrite = prepareSumm(dataread)
prevdayw = gsub("714","KH-714S",prevday)
dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t")
print('Summary read')
if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
{
print('Difference in old file noted')



previdx = (last30daysidx - 1) %% 31

if(previdx == 0) {previdx = 1}
  datasum = rewriteSumm(datawrite)
prevdayw = gsub("714","KH-714S",prevday)
  write.table(datawrite,file = paste(prevwritemonths,prevdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)


if(prevpathmonth == pathmonth)
{
	monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
  monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
	monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
	monthlyirr4 = monthlyirr4 + as.numeric(datawrite[1,6]) - as.numeric(dataprev[1,6])
}
globirr1 = globirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
globirr2 = globirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
globirr3 = globirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
globirr4 = globirr4 + as.numeric(datawrite[1,6]) - as.numeric(dataprev[1,6])

last30days[[previdx]] = as.numeric(datawrite[1,3])
last30days2[[previdx]] = as.numeric(datawrite[1,6])



datarw = read.table(paste(prevwritemonths,prevsumname,sep="/"),header = T,sep = "\t")
  rn = match(as.character(datawrite[,1]),as.character(datarw[,1]))
  print(paste('Match found at row no',rn))
  datarw[rn,] = datasum[1,]
  write.table(datarw,file = paste(prevwritemonths,prevsumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
  filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
  filename[2] = prevday
  filedesc[2] = c("Updated Digest")
print('Updated old files.. both digest and summary file')
}

last30daysidx = (last30daysidx + 1 ) %% 31
if(last30daysidx == 0) {last30daysidx = 1}

LAST30DAYSTOT = rf(sum(last30days))
LAST30DAYSMEAN = rf(mean(last30days))
LAST30DAYSTOT2 = rf(sum(last30days2))
LAST30DAYSMEAN2 = rf(mean(last30days2))
DAYSACTIVE = daysactive
MONTHLYIRR1 = monthlyirr1
MONTHLYIRR2 = monthlyirr2
MONTHLYIRR3 = monthlyirr3
MONTHLYIRR4 = monthlyirr4
MONTHAVG1 = rf(monthlyirr1 / dayssofarmonth)
MONTHAVG2 = rf(monthlyirr2 / dayssofarmonth)
MONTHAVG3 = rf(monthlyirr3 / dayssofarmonth)
MONTHAVG4 = rf(monthlyirr4 / dayssofarmonth)

FORECASTIRR1 = rf(as.numeric(MONTHAVG1) * daystruemonth)
FORECASTIRR2 = rf(as.numeric(MONTHAVG2) * daystruemonth)
FORECASTIRR3 = rf(as.numeric(MONTHAVG3) * daystruemonth)
FORECASTIRR4 = rf(as.numeric(MONTHAVG4) * daystruemonth)

SOILINGDEC = rf3(((globirr2 / globirr1) - 1) * 100)
SNPDEC = rf3(((globirr3 / globirr2) - 1) * 100)
SOILINGDEC2 = rf3(((globirr3 / globirr1) - 1) * 100)

SOILINGDECPD = rf3(as.numeric(SOILINGDEC) / DAYSACTIVE)
SOILINGDECPD2 = rf3(as.numeric(SOILINGDEC2) / DAYSACTIVE)

SNPDECPD = rf3(as.numeric(SNPDEC) / DAYSACTIVE)

print('Sending mail')
body = ""
body = preparebody(filetosendpath)
body = gsub("\n ","\n",body)
send.mail(from = sender,
          to = recipients,
          subject = paste("Station [KH-714S] Digest",substr(currday,7,16)),
          body = body,
          smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
          authenticate = TRUE,
          send = TRUE,
          attach.files = filetosendpath,
          file.names = filename, # optional parameter
          file.descriptions = filedesc, # optional parameter
          debug = F)
print('Mail Sent')
recordTimeMaster("KH-714S","Mail",substr(currday,7,16))
prevday = currday
prevpathyear = pathyear
prevpathmonth = pathmonth
prevwriteyears = writeyears
prevwritemonths = writemonths
prevsumname = sumname
wt = 1
gc()
}
sink()
