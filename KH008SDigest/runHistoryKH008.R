rm(list = ls(all = T))
source('/home/admin/CODE/common/math.R')
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf1 = function(x)
{
  return(format(round(as.numeric(x),1),nsmall=1))
}
rf = function(x)
{
  return(format(round(as.numeric(x),2),nsmall=2))
}
prepareSumm = function(dataread)
{
  da = nrow(dataread)
  daPerc = round(da/14.4,1)
  thresh = 5
  gsi1 = sum(dataread[complete.cases(dataread[,4]),4])/60000
  gsi2 = sum(dataread[complete.cases(dataread[,5]),5])/60000
  gsismp = sum(dataread[complete.cases(dataread[,3]),3])/60000
  subdata = dataread[complete.cases(dataread[,4]),]
  subdata = subdata[as.numeric(subdata[,4]) > thresh,]
  tamb = mean(dataread[complete.cases(dataread[,9]),9])
  tambst = mean(subdata[,9])
  
  hamb = mean(dataread[complete.cases(dataread[,10]),10])
  hambst = mean(subdata[,10])
  
  tambmx = max(dataread[complete.cases(dataread[,9]),9])
  tambstmx = max(subdata[,9])
  
  hambmx = max(dataread[complete.cases(dataread[,10]),10])
  hambstmx = max(subdata[,10])
  
  tambmn = min(dataread[complete.cases(dataread[,9]),9])
  tambstmn = min(subdata[,9])
  
  hambmn = min(dataread[complete.cases(dataread[,10]),10])
  hambstmn = min(subdata[,10])
  
  tsi1 = mean(dataread[complete.cases(dataread[,7]),7])
  tsi1min = min(dataread[complete.cases(dataread[,7]),7])
  tsi1max = max(dataread[complete.cases(dataread[,7]),7])
  
  tsi1st = mean(subdata[complete.cases(subdata[,7]),7])
  tsi1minst = min(subdata[complete.cases(subdata[,7]),7])
  tsi1maxst = max(subdata[complete.cases(subdata[,7]),7])
  
  ws = mean(dataread[complete.cases(dataread[,19]),19])
  sdws = sdp(dataread[complete.cases(dataread[,19]),19])
  covws = sdws*100/ws
  wsmax = max(dataread[complete.cases(dataread[,19]),19])
  wssh = mean(subdata[complete.cases(subdata[,19]),19])
  
  wd = mean(dataread[complete.cases(dataread[,20]),20])
  sdwd = sdp(dataread[complete.cases(dataread[,20]),20])
  covwd = sdwd*100/wd
  wdst = mean(subdata[complete.cases(subdata[,20]),20])
  
  gsirat = gsi2 / gsi1
  smprat = gsismp / gsi1
  
  LastPond = LastR06 = LastR02 = LastR03 = LastR11 = LastTime = NA
  
  dataread2 = as.numeric(dataread[complete.cases(as.numeric(dataread[,55])),55])
  cond = ( dataread2 > 0 &  dataread2 < 1000000000)
	dataread2 =  dataread2[cond]
  if(length(dataread2))
    LastPond = as.numeric(dataread2[length(dataread2)])
  
  dataread2 = as.numeric(dataread[complete.cases(as.numeric(dataread[,102])),102])
  cond = ( dataread2 > 0 &  dataread2 < 1000000000)
	dataread2 =  dataread2[cond]
  if(length(dataread2))
    LastR06 = as.numeric(dataread2[length(dataread2)])
  
  dataread2 = as.numeric(dataread[complete.cases(as.numeric(dataread[,149])),149])
  cond = ( dataread2 > 0 &  dataread2 < 1000000000)
	dataread2 =  dataread2[cond]
  if(length(dataread2))
    LastR02 = as.numeric(dataread2[length(dataread2)])
  
  dataread2 = as.numeric(dataread[complete.cases(as.numeric(dataread[,196])),196])
  cond = ( dataread2 > 0 &  dataread2 < 1000000000)
	dataread2 =  dataread2[cond]
  if(length(dataread2))
    LastR03 = as.numeric(dataread2[length(dataread2)])
  
  dataread2 = as.numeric(dataread[complete.cases(as.numeric(dataread[,243])),243])
  cond = ( dataread2 > 0 &  dataread2 < 1000000000)
	dataread2 =  dataread2[cond]
  if(length(dataread2))
    LastR11 = as.numeric(dataread2[length(dataread2)])
  
  LastTime = as.character(dataread[nrow(dataread),1])
  
  Eac1Pond = sum(as.numeric(dataread[complete.cases(as.numeric(dataread[,40])),40]))/60
  Eac1R06 = sum(as.numeric(dataread[complete.cases(as.numeric(dataread[,87])),87]))/60
  Eac1R02 = sum(as.numeric(dataread[complete.cases(as.numeric(dataread[,134])),134]))/60
  Eac1R03 = sum(as.numeric(dataread[complete.cases(as.numeric(dataread[,181])),181]))/60
  Eac1R11 = sum(as.numeric(dataread[complete.cases(as.numeric(dataread[,228])),228]))/60
  
  time = as.character(dataread[,1])
  time = time[complete.cases(as.numeric(dataread[,55]))]
  Eac2Pond = as.numeric(dataread[complete.cases(as.numeric(dataread[,55])),55])
  {
    if(length(Eac2Pond))
    {
      LastReadPond = Eac2Pond[length(Eac2Pond)]
      Eac2Pond = (Eac2Pond[length(Eac2Pond)] - Eac2Pond[1])
      LastTimePond = as.character(time[length(time)])
    }
    else
    {
      Eac2Pond = LastReadPond=LastTimePond = NA
    }
  }
  
  time = as.character(dataread[,1])
  time = time[complete.cases(as.numeric(dataread[,102]))]
  Eac2R06 = as.numeric(dataread[complete.cases(as.numeric(dataread[,102])),102])
  {
    if(length(Eac2R06))
    {
      LastReadR06 = Eac2R06[length(Eac2R06)]
      Eac2R06 = (Eac2R06[length(Eac2R06)] - Eac2R06[1])
      LastTimeR06 = as.character(time[length(time)])
    }
    else
    {
      Eac2R06 = LastReadR06=LastTimeR06 = NA
    }
  }
  
  time = as.character(dataread[,1])
  time = time[complete.cases(as.numeric(dataread[,149]))]
  Eac2R02 = as.numeric(dataread[complete.cases(as.numeric(dataread[,149])),149])
  {
    if(length(Eac2R02))
    {
      LastReadR02 = Eac2R02[length(Eac2R02)]
      Eac2R02 = (Eac2R02[length(Eac2R02)] - Eac2R02[1])
      LastTimeR02 = as.character(time[length(time)])
    }
    else
    {
      Eac2R02 = LastReadR02=LastTimeR02 = NA
    }
  }
  
  time = as.character(dataread[,1])
  time = time[complete.cases(as.numeric(dataread[,196]))]
  Eac2R03 = as.numeric(dataread[complete.cases(as.numeric(dataread[,196])),196])
  {
    if(length(Eac2R03))
    {
      LastReadR03 = Eac2R03[length(Eac2R03)]
      Eac2R03 = (Eac2R03[length(Eac2R03)] - Eac2R03[1])
      LastTimeR03 = as.character(time[length(time)])
    }
    else
    {
      Eac2R03 = LastReadR03=LastTimeR03 = NA
    }
  }
  
  time = as.character(dataread[,1])
  time = time[complete.cases(as.numeric(dataread[,243]))]
  Eac2R11 = as.numeric(dataread[complete.cases(as.numeric(dataread[,243])),243])
  {
    if(length(Eac2R11))
    {
      LastReadR11 = Eac2R11[length(Eac2R11)]
      Eac2R11 = (Eac2R11[length(Eac2R11)] - Eac2R11[1])
      LastTimeR11 = as.character(time[length(time)])
    }
    else
    {
      Eac2R11 = LastReadR11=LastTimeR11 = NA
    }
  }
  
  
  Yld1Pond = Eac1Pond / 2835.32
  Yld1R06 = Eac1R06 / 495.95
  Yld1R11 = Eac1R11 / 1397.50
  Yld1R03 = Eac1R03 / 2854.80
  Yld1R02 = Eac1R02 / 2249.98
  
  Yld2Pond = Eac2Pond / 2835.32
  Yld2R06 = Eac2R06 / 495.95
  Yld2R11 = Eac2R11 / 1397.50
  Yld2R03 = Eac2R03 / 2854.80
  Yld2R02 = Eac2R02 / 2249.98
  
  PR1Pond = Yld1Pond * 100 / gsi1
  PR1R06 = Yld1R06 * 100 / gsi1
  PR1R11 = Yld1R11 * 100 / gsi1
  PR1R03 = Yld1R03 * 100 / gsi1
  PR1R02 = Yld1R02 * 100 / gsi1 
  
  PR2Pond = Yld2Pond * 100 / gsi1
  PR2R06 = Yld2R06 * 100 / gsi1
  PR2R11 = Yld2R11 * 100 / gsi1
  PR2R03 = Yld2R03 * 100 / gsi1
  PR2R02 = Yld2R02 * 100 / gsi1 
  
  Eac1Full =  Eac1R02 + Eac1R03 + Eac1R06 + Eac1R11 + Eac1Pond
  Eac2Full =  Eac2R02 + Eac2R03 + Eac2R06 + Eac2R11 + Eac2Pond
  Yld1Full = Eac1Full / 9833.55
  Yld2Full = Eac2Full / 9833.55
  PR1Full = Yld1Full *100 / gsi1
  PR2Full = Yld2Full *100 / gsi1
  
  timestamps_irradiance_greater_20 = dataread[dataread[,4]>20,1]
  
  #GA Pond
  timestamps_freq_greater_40 = dataread[dataread[,53]>40,1]
  timestamps_pow_greater_2 = dataread[dataread[,40]>2,1]
  common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)
  common2 = intersect(common, timestamps_pow_greater_2)
  GA_Pond = (length(common)/length(timestamps_irradiance_greater_20))*100
  GA_Pond = round(GA_Pond, 2)
  PA_Pond = (length(common2)/length(timestamps_irradiance_greater_20))*100
  PA_Pond = round(PA_Pond, 2)
  
  #GA R06
  timestamps_freq_greater_40 = dataread[dataread[,100]>40,1]
  timestamps_pow_greater_2 = dataread[dataread[,87]>2,1]
  common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)
  common2 = intersect(common, timestamps_pow_greater_2)
  GA_R06 = (length(common)/length(timestamps_irradiance_greater_20))*100
  GA_R06 = round(GA_R06, 2)
  PA_R06 = (length(common2)/length(timestamps_irradiance_greater_20))*100
  PA_R06 = round(PA_R06, 2)
  
  #GA R11
  timestamps_freq_greater_40 = dataread[dataread[,241]>40,1]
  timestamps_pow_greater_2 = dataread[dataread[,228]>2,1]
  common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)
  common2 = intersect(common, timestamps_pow_greater_2)
  GA_R11=(length(common)/length(timestamps_irradiance_greater_20))*100
  GA_R11 = round(GA_R11, 2)
  PA_R11 = (length(common2)/length(timestamps_irradiance_greater_20))*100
  PA_R11 = round(PA_R11, 2)
  
  #GA R03
  timestamps_freq_greater_40 = dataread[dataread[,194]>40,1]
  timestamps_pow_greater_2 = dataread[dataread[,181]>2,1]
  common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)
  common2 = intersect(common, timestamps_pow_greater_2)
  GA_R03=(length(common)/length(timestamps_irradiance_greater_20))*100
  GA_R03 = round(GA_R03, 2)
  PA_R03 = (length(common2)/length(timestamps_irradiance_greater_20))*100
  PA_R03 = round(PA_R03, 2)
  
  #GA R02
  timestamps_freq_greater_40 = dataread[dataread[,147]>40,1]
  timestamps_pow_greater_2 = dataread[dataread[,134]>2,1]
  common = intersect(timestamps_irradiance_greater_20, timestamps_freq_greater_40)
  common2 = intersect(common, timestamps_pow_greater_2)
  GA_R02=(length(common)/length(timestamps_irradiance_greater_20))*100
  GA_R02 = round(GA_R02, 2)
  PA_R02 = (length(common2)/length(timestamps_irradiance_greater_20))*100
  PA_R02 = round(PA_R02, 2)
  
  
  datawrite = data.frame(Date = substr(dataread[1,1],1,10),PtsRec = rf(da),Gsi01 = rf(gsi1), Gsi02 = rf(gsi2),Smp = rf(gsismp),
                         Tamb = rf1(tamb), TambSH = rf1(tambst),TambMx = rf1(tambmx), TambMn = rf1(tambmn),
                         TambSHmx = rf1(tambstmx), TambSHmn = rf1(tambstmn), Hamb = rf1(hamb), HambSH = rf1(hambst),
                         HambMx = rf1(hambmx), HambMn = rf1(hambmn), HambSHmx = rf1(hambstmx), HambSHmn = rf1(hambstmn),
                         TMod01 = rf1(tsi1), GsiRat = rf(gsirat), SpmRat = rf(smprat),WindDir=rf1(wd),
                         WindDirSH=rf1(wdst),TModSH=rf1(tsi1st),TmodMax=rf1(tsi1max),TModMaxSH=rf1(tsi1maxst),TModMin=rf1(tsi1min),TModMinST=rf1(tsi1minst),
                         WindSpeedMean=rf1(ws),WindSpeedMax=rf1(wsmax),WindSpeedMeanSH=rf1(wssh),
                         SDWindSpd = rf1(sdws), CovWindSpd=rf1(covws), SDWindDir=rf1(sdwd),CovWindDir=rf1(covwd),
                         DA=rf1(daPerc),
                         Eac1Pond = rf(Eac1Pond), Eac2Pond = rf(Eac2Pond), Yld1Pond = rf(Yld1Pond), Yld2Pond = rf(Yld2Pond), 
                         PR1Pond = rf1(PR1Pond), PR2Pond = rf1(PR2Pond), LastReadPond = rf1(LastReadPond), LastTimePond = LastTimePond,
                         Eac1R02 = rf(Eac1R02), Eac2R02 = rf(Eac2R02), Yld1R02 = rf(Yld1R02), Yld2R02 = rf(Yld2R02), 
                         PR1R02 = rf1(PR1R02), PR2R02 = rf1(PR2R02), LastReadR02 = rf1(LastReadR02), LastTimeR02 = LastTimeR02,
                         Eac1R03 = rf(Eac1R03), Eac2R03 = rf(Eac2R03), Yld1R03 = rf(Yld1R03), Yld2R03 = rf(Yld2R03), 
                         PR1R03 = rf1(PR1R03), PR2R03 = rf1(PR2R03), LastReadR03 = rf1(LastReadR03), LastTimeR03 = LastTimeR03,
                         Eac1R06 = rf(Eac1R06), Eac2R06 = rf(Eac2R06), Yld1R06 = rf(Yld1R06), Yld2R06 = rf(Yld2R06), 
                         PR1R06 = rf1(PR1R06), PR2R06 = rf1(PR2R06), LastReadR06 = rf1(LastReadR06), LastTimeR06 = LastTimeR06,
                         Eac1R11 = rf(Eac1R11), Eac2R11 = rf(Eac2R11), Yld1R11 = rf(Yld1R11), Yld2R11 = rf(Yld2R11), 
                         PR1R11 = rf1(PR1R11), PR2R11 = rf1(PR2R11), LastReadR11 = rf1(LastReadR11), LastTimeR11 = LastTimeR11,
                         Eac1Full = rf(Eac1Full), Eac2Full = rf(Eac2Full), Yld1Full = rf(Yld1Full), Yld2Full = rf(Yld2Full),
                         PR1Full = rf1(PR1Full), PR2Full = rf1(PR2Full), 
                         GA_Pond = GA_Pond, GA_R06 = GA_R06, GA_R11 = GA_R11, GA_R03 = GA_R03, GA_R02 = GA_R02,
                         PA_Pond = PA_Pond, PA_R06 = PA_R06, PA_R11 = PA_R11, PA_R03 = PA_R03, PA_R02 = PA_R02,
                         stringsAsFactors=F)
  datawrite
}

rewriteSumm = function(datawrite)
{
  Eac1Pond = as.character(datawrite[1,36])
  Eac1R02 = as.character(datawrite[1,44])
  Eac1R03 = as.character(datawrite[1,52])
  Eac1R06 = as.character(datawrite[1,60])
  Eac1R11 = as.character(datawrite[1,68])
  
  Eac2Pond = as.character(datawrite[1,37])
  Eac2R02 = as.character(datawrite[1,45])
  Eac2R03 = as.character(datawrite[1,53])
  Eac2R06 = as.character(datawrite[1,61])
  Eac2R11 = as.character(datawrite[1,69])
  
  Yld1Pond = as.character(datawrite[1,38])
  Yld1R02 =as.character(datawrite[1,46]) 
  Yld1R03 = as.character(datawrite[1,54])
  Yld1R06 = as.character(datawrite[1,62])
  Yld1R11 = as.character(datawrite[1,70])
  
  Yld2Pond = as.character(datawrite[1,39])
  Yld2R02 =as.character(datawrite[1,47]) 
  Yld2R03 = as.character(datawrite[1,55])
  Yld2R06 = as.character(datawrite[1,63])
  Yld2R11 = as.character(datawrite[1,71])
  
  PR1Pond = as.character(datawrite[1,40])
  PR1R02 = as.character(datawrite[1,48])
  PR1R03 = as.character(datawrite[1,56])
  PR1R06 = as.character(datawrite[1,64])
  PR1R11 = as.character(datawrite[1,72])
  
  PR2Pond = as.character(datawrite[1,41])
  PR2R02 = as.character(datawrite[1,49])
  PR2R03 = as.character(datawrite[1,57])
  PR2R06 = as.character(datawrite[1,65])
  PR2R11 = as.character(datawrite[1,73])
  
  LastReadPond = as.character(datawrite[1,42])
  LastReadR02 = as.character(datawrite[1,50])
  LastReadR03 = as.character(datawrite[1,58])
  LastReadR06 = as.character(datawrite[1,66])
  LastReadR11 = as.character(datawrite[1,74])
  
  LastTimePond = as.character(datawrite[1,43])
  LastTimeR02 = as.character(datawrite[1,51])
  LastTimeR03 = as.character(datawrite[1,59])
  LastTimeR06 = as.character(datawrite[1,67])
  LastTimeR11 = as.character(datawrite[1,75])
  
  Eac1Full = as.character(datawrite[1,76])
  Eac2Full = as.character(datawrite[1,77])
  Yld1Full = as.character(datawrite[1,78])
  Yld2Full = as.character(datawrite[1,79])
  PR1Full = as.character(datawrite[1,80])
  PR2Full = as.character(datawrite[1,81])
  
  df = data.frame(Date = as.character(datawrite[1,1]),Gsi01 = as.character(datawrite[1,3]),Gsi02 = as.character(datawrite[1,4]),Smp = as.character(datawrite[1,5]),
                  Tamb = as.character(datawrite[1,6]),TambSH = as.character(datawrite[1,7]),Hamb = as.character(datawrite[1,12]),HambSH = as.character(datawrite[,13]),
                  Eac1Pond = rf(Eac1Pond), Eac2Pond = rf(Eac2Pond), Yld1Pond = rf(Yld1Pond), Yld2Pond = rf(Yld2Pond), 
                  PR1Pond = rf1(PR1Pond), PR2Pond = rf1(PR2Pond), LastReadPond = rf1(LastReadPond), LastTimePond = LastTimePond,
                  Eac1R02 = rf(Eac1R02), Eac2R02 = rf(Eac2R02), Yld1R02 = rf(Yld1R02), Yld2R02 = rf(Yld2R02), 
                  PR1R02 = rf1(PR1R02), PR2R02 = rf1(PR2R02), LastReadR02 = rf1(LastReadR02), LastTimeR02 = LastTimeR02,
                  Eac1R03 = rf(Eac1R03), Eac2R03 = rf(Eac2R03), Yld1R03 = rf(Yld1R03), Yld2R03 = rf(Yld2R03), 
                  PR1R03 = rf1(PR1R03), PR2R03 = rf1(PR2R03), LastReadR03 = rf1(LastReadR03), LastTimeR03 = LastTimeR03,
                  Eac1R06 = rf(Eac1R06), Eac2R06 = rf(Eac2R06), Yld1R06 = rf(Yld1R06), Yld2R06 = rf(Yld2R06), 
                  PR1R06 = rf1(PR1R06), PR2R06 = rf1(PR2R06), LastReadR06 = rf1(LastReadR06), LastTimeR06 = LastTimeR06,
                  Eac1R11 = rf(Eac1R11), Eac2R11 = rf(Eac2R11), Yld1R11 = rf(Yld1R11), Yld2R11 = rf(Yld2R11), 
                  PR1R11 = rf1(PR1R11), PR2R11 = rf1(PR2R11), LastReadR11 = rf1(LastReadR11), LastTimeR11 = LastTimeR11,
                  Eac1Full = rf(Eac1Full), Eac2Full = rf(Eac2Full), Yld1Full = rf(Yld1Full), Yld2Full = rf(Eac2Full),
                  PR1Full = rf1(PR1Full), PR2Full = rf1(PR2Full),
                  #GA_Pond = GA_Pond, GA_R06 = GA_R06, GA_R11 = GA_R11, GA_R03 = GA_R03, GA_R02 = GA_R02,
                  
                  stringsAsFactors=F)
  df
}

getCustData = function(data)
{
  date = as.character(data[,1])
  Irr = as.character(data[,2])
  EacPond = as.character(data[,10])
  EacR02 = as.character(data[,18])
  EacR03 = as.character(data[,26])
  EacR06 = as.character(data[,34])
  EacR11 = as.character(data[,42])
  EacTotal = as.character(data[,50])
  df = data.frame(Date=date,Irr=Irr,EacPond=EacPond,EacR02=EacR02,EacR03 = EacR03,
                  EacR06=EacR06,EacR11=EacR11,EacTotal=EacTotal,stringsAsFactors=F)
  return(df)
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[728]"
pathwrite = "/home/admin/Dropbox/Second Gen/[KH-008S]" 

pathwriteCust = "/home/admin/Dropbox/Customer/[KH-9008S]"

checkdir(pathwrite)
checkdir(pathwriteCust)

years = dir(path)
idxgs = match("_gsdata_",years)
if(is.finite(idxgs))
  years = years[-idxgs]
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  writeyearCust = paste(pathwriteCust,years[x],sep="/")
  checkdir(writeyear)
  checkdir(writeyearCust)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[KH-008S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    sumfilenameCust = paste("[KH-9008S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T,stringsAsFactors=F)
      datawrite = prepareSumm(dataread)
      datasum = rewriteSumm(datawrite)
      
      datasumCust = getCustData(datasum)
      
      currdayw = gsub("728","KH-008S",days[z])
      
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
      {
        if(!file.exists(paste(writeyearCust,sumfilenameCust,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasumCust,file = paste(writeyearCust,sumfilenameCust,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasumCust,file = paste(writeyearCust,sumfilenameCust,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}